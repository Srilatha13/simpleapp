import math

def add(x, y):
    return x + y

def subtract(x, y):
    return x - y

def multiply(x, y):
    return x * y

def divide(x, y):
    if y == 0:
        return "Cannot divide by zero"
    else:
        return x / y

def logarithm(x):
    if x <= 0:
        return "Invalid input"
    else:
        return math.log(x)

def sine(x):
    return math.sin(math.radians(x))

def cosine(x):
    return math.cos(math.radians(x))

def tangent(x):
    return math.tan(math.radians(x))

def calculator():
    print("Select operation:")
    print("1. Addition")
    print("2. Subtraction")
    print("3. Multiplication")
    print("4. Division")
    print("5. Logarithm")
    print("6. Sine")
    print("7. Cosine")
    print("8. Tangent")

    choice = input("Enter choice (1/2/3/4/5/6/7/8): ")

    if choice in ('1', '2', '3', '4'):
        num1 = float(input("Enter first number: "))
        num2 = float(input("Enter second number: "))
        if choice == '1':
            print("Result:", add(num1, num2))
        elif choice == '2':
            print("Result:", subtract(num1, num2))
        elif choice == '3':
            print("Result:", multiply(num1, num2))
        elif choice == '4':
            print("Result:", divide(num1, num2))
    elif choice in ('5', '6', '7', '8'):
        num = float(input("Enter number: "))
        if choice == '5':
            print("Result:", logarithm(num))
        elif choice == '6':
            print("Result:", sine(num))
        elif choice == '7':
            print("Result:", cosine(num))
        elif choice == '8':
            print("Result:", tangent(num))
    else:
        print("Invalid input")

if __name__ == "__main__":
    calculator()